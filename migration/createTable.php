<?php
require_once '../config/db_connection.php';

try{
    $sql = "CREATE TABLE persons (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(255) NOT NULL,
    second_name VARCHAR(255) NOT NULL,
    phone VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    role VARCHAR(255) NOT NULL,
    average_mark FLOAT,
    subject VARCHAR(255),
    working_day VARCHAR(255)) DEFAULT CHARACTER  SET utf8 ENGINE=InnoDB";
    $pdo->exec($sql);
}catch (Exception $exception){
    echo "Error creating table! " . $exception->getCode() . ' message: ' . $exception->getMessage();
    die();
}
