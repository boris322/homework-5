<?php
require_once '../config/db_connection.php';

try{
    $query = "INSERT INTO persons(first_name,second_name, phone, email, role, average_mark, subject, working_day)
        VALUES
        ('Richard','Hendricks',3654432,'rich@email.net','Student', 5, null ,null),
        ('Richard','Hendricks',36544323,'rich@email.net','Teacher',null,'PHP',null),
        ('Richard','Hendricks',3654433,'rich@email.net','Admin',null,null,'Saturday')
        ";
    $pdo->exec($query);
}catch (Exception $exception){
    echo "Fixtures error" . $exception->getCode() . 'message: ' . $exception->getMessage();
    die();
}
header('Location../admin/viewUser.php');