<?php
require_once 'config/db_connection.php';
require_once 'classes/Person.php';
$role = htmlspecialchars($_GET['userRole']);

switch ($role){
    case 'Admin':
        $userObjs = Admin::getByRole($role,$pdo);
        break;
    case 'Teacher':
        $userObjs = Teacher::getByRole($role,$pdo);
        break;
    case 'Student':
        $userObjs = Student::getByRole($role,$pdo);
        break;
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!--Bootstrap Style-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
          crossorigin="anonymous">
    <style>
        .table{margin-top: 25px;}
    </style>
</head>
<body>
<div class="container-md">
    <nav class="navbar navbar-dark bg-dark">
        <ul class="nav nav-pills">
            <li class="nav-item">
                <a class="navbar-brand" href="../index.php">HomeWork5</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="../index.php" role="button" aria-haspopup="true" aria-expanded="false">Admin</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="/admin/createUser.php">Add new user</a>
                    <a class="dropdown-item" href="/admin/viewUser.php">View all users</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/migration/createTable.php">Create Table</a>
                    <a class="dropdown-item" href="/migration/fixtures.php">Fixtures DB</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="viewUsersByRole.php?role=No">View users by role</a>
            </li>
        </ul>
    </nav>
    <!--Main-->
    <?php if ($_GET['role'] == 'No'):?>
        <div>
            <h1>Choose role</h1>
            <a class="btn btn-success" href="viewUsersByRole.php?role=Yes&userRole=Admin" role="button">Admin</a>
            <a class="btn btn-success" href="viewUsersByRole.php?role=Yes&userRole=Teacher" role="button">Teacher</a>
            <a class="btn btn-success" href="viewUsersByRole.php?role=Yes&userRole=Student" role="button">Student</a>
        </div>
    <?php endif;?>
    <?php if ($_GET['role'] == 'Yes' && isset($_GET['role'])):?>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">First name</th>
            <th scope="col">Second Name</th>
            <th scope="col">Role</th>
            <th scope="col"></th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
            <?php foreach ($userObjs as $userObj):?>
            <tr>
                <td><?= $userObj->getFirstName(); ?></td>
                <td><?= $userObj->getSecondName(); ?></td>
                <td><?= $userObj->getRole(); ?></td>
                <form action="moreDetails.php" method="GET">
                    <input type="hidden" name="userId" value="<?= $userObj->getId(); ?>">
                    <input type="hidden" name="userRole" value="<?= $userObj->getRole(); ?>">
                    <td><button type="submit" class="btn btn-warning">More details</button></td>
                </form>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>
</div>







<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>