<?php
if (empty($_POST['userId'])){
    header('Location:../index.php');
}
require_once '../config/db_connection.php';
require_once '../classes/Person.php';

$id = htmlspecialchars($_POST['userId']);
$role = htmlspecialchars($_POST['userRole']);
switch ($role){
    case 'Admin':
        $userObj = Admin::getById($id,$pdo);
        break;
    case 'Teacher':
        $userObj = Teacher::getById($id,$pdo);
        break;
    case 'Student':
        $userObj = Student::getById($id,$pdo);
        break;
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
          crossorigin="anonymous">
</head>
<body>
    <div class="container-md">
        <nav class="navbar navbar-dark bg-dark">
            <ul class="nav nav-pills">
                <li class="nav-item">
                    <a class="navbar-brand" href="../index.php">HomeWork5</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Admin</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="/admin/createUser.php">Add new user</a>
                        <a class="dropdown-item" href="/admin/viewUser.php">View all users</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="/migration/createTable.php">Create Table</a>
                        <a class="dropdown-item" href="/migration/fixtures.php">Fixtures DB</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../viewUsersByRole.php?role=No">View users by role</a>
                </li>
            </ul>
        </nav>
        <form action="updateUser.php" method="post">
            <input name="userId" type="hidden" value="<?= $userObj->getId(); ?>">
            <div class="form-group">
                <label for="exampleFormControlInput1">Enter First Name</label>
                <input value="<?=$userObj->getFirstName(); ?>" name="firstName" type="text" class="form-control" id="exampleFormControlInput1" >
            </div><div class="form-group">
                <label for="exampleFormControlInput2">Enter Second Name</label>
                <input value="<?= $userObj->getSecondName(); ?>" name="secondName" type="text" class="form-control" id="exampleFormControlInput2" >
            </div>
            <div class="form-group">
                <label for="exampleFormControlInput3">Enter Email address</label>
                <input value="<?= $userObj->getEmail(); ?>" name="email" type="email" class="form-control" id="exampleFormControlInput3" >
            </div>
            <div class="form-group">
                <label for="exampleFormControlInput4">Enter phone number</label>
                <input  value="<?= $userObj->getPhoneNumber(); ?>" name="phoneNumber" type="text" class="form-control" id="exampleFormControlInput4" >
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1">Enter role</label>
                <select name="role" class="form-control" id="exampleFormControlSelect1">
                    <option selected><?= $userObj->getRole(); ?></option>
                </select>
            </div>
            <?php switch ($userObj->getRole()):
                case 'Admin':?>
                    <div class="form-group">
                        <label for="exampleFormControlSelect5">Select working days for Administrator</label>
                        <select name="workDay"  class="form-control" id="exampleFormControlSelect5">
                            <option  selected ><?= $userObj->getDayWork(); ?></option>
                            <option>Monday</option>
                            <option >Tuesday</option>
                            <option >Wednesday</option>
                            <option >Thursday</option>
                            <option >Friday</option>
                        </select>
                    </div>
                    <?php break; ?>

                <?php case 'Teacher':?>
                    <div class="form-group">
                        <label for="exampleFormControlInput5">Enter subject</label>
                        <input  value="<?= $userObj->getSubject(); ?>" name="subject" type="text" class="form-control" id="exampleFormControlInput5" >
                    </div>
                    <?php break; ?>

                <?php case 'Student':?>
                    <div class="form-group">
                        <label for="exampleFormControlInput5">Enter average mark</label>
                        <input value="<?= $userObj->getAverageMark(); ?>" name="averageMark" type="text" class="form-control" id="exampleFormControlInput5" >
                    </div>
                    <small id="passwordHelpBlock" class="form-text text-muted">
                        Maximum score may be 10.
                    </small>
                    <?php break; ?>
                <?php endswitch; ?>
            <button type="submit" class="btn btn-success">Success</button>

        </form>

    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>

