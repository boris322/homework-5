<?php
require_once '../config/db.php';
require_once '../config/db_connection.php';
require_once '../classes/Person.php';
require_once '../classes/Admin.php';
require_once '../classes/Student.php';
require_once '../classes/Teacher.php';

if (isset($_POST['userId'])) {
    $role = htmlspecialchars($_POST['role']);
    switch ($role){
        case 'Admin':
            $userObjs = Admin::upgrade($_POST['userId'], $_POST['firstName'], $_POST['secondName'],
                $_POST['email'], $_POST['phoneNumber'],$_POST['role'], $_POST['workDay'], $pdo);
            break;
        case 'Teacher':
            $userObjs = Teacher::upgrade($_POST['userId'], $_POST['firstName'], $_POST['secondName'],
                $_POST['email'], $_POST['phoneNumber'],$_POST['role'], $_POST['subject'], $pdo);
            break;
        case 'Student':
            $userObjs = Student::upgrade($_POST['userId'], $_POST['firstName'], $_POST['secondName'],
                $_POST['email'], $_POST['phoneNumber'],$_POST['role'], $_POST['averageMark'], $pdo);
            break;
    }

    header('Location: ../index.php');
}
