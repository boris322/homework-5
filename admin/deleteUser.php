<?php
if (empty($_POST['userId'])){
    header('Location:/admin/viewUser.php');
    die();
}
require_once '../config/db.php';
require_once '../config/db_connection.php';
require_once '../classes/Person.php';

switch ($_POST['userRole']){
    case 'Admin':
        Admin::delete($_POST['userId'], $pdo);
        break;
    case 'Teacher':
        Teacher::delete($_POST['userId'], $pdo);
        break;
    case 'Student':
        Student::delete($_POST['userId'], $pdo);
        break;
}

header('Location:/admin/viewUser.php');