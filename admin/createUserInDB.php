<?php
require_once '../config/db.php';
require_once '../config/db_connection.php';
require_once '../classes/Person.php';
require_once '../classes/Admin.php';
require_once '../classes/Student.php';
require_once '../classes/Teacher.php';

if (!empty($_POST['firstName'])){

    $userObjs = [];
    switch ($_POST['role']){
        case 'Admin':
            $userObjs[] = new  Admin($_POST['firstName'], $_POST['secondName'],$_POST['phoneNumber'], $_POST['email'],$_POST['role'], $_POST['workDays']);
            break;
        case 'Teacher':
            $userObjs[] = new  Teacher($_POST['firstName'], $_POST['secondName'],$_POST['phoneNumber'], $_POST['email'],$_POST['role'], $_POST['subject']);
            break;
        case 'Student':
            $userObjs[] = new  Student($_POST['firstName'], $_POST['secondName'],$_POST['phoneNumber'], $_POST['email'],$_POST['role'], $_POST['averageMark']);
            break;
    }
    foreach ($userObjs as $userObj) {
        $userObj->createUser($pdo);
    }

    header('Location:viewUser.php');
}