<?php
if (empty($_GET['userId'])){
    header('Location:index.php');
}
require_once 'classes/Person.php';
require_once 'classes/Admin.php';
require_once 'classes/Student.php';
require_once 'classes/Teacher.php';

require_once 'config/db.php';
require_once 'config/db_connection.php';

$id = htmlspecialchars($_GET['userId']);
$role = htmlspecialchars($_GET['userRole']);

switch ($role){
    case 'Admin':
        $userObj = Admin::getById($id,$pdo);
        break;
    case 'Teacher':
        $userObj = Teacher::getById($id,$pdo);
        break;
    case 'Student':
        $userObj = Student::getById($id,$pdo);
        break;
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!--Bootstrap Style-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
          crossorigin="anonymous">
</head>
<body>
<div class="container-md">
    <nav class="navbar navbar-dark bg-dark">
        <ul class="nav nav-pills">
            <li class="nav-item">
                <a class="navbar-brand" href="index.php">HomeWork5</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Admin</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="/admin/createUser.php">Add new user</a>
                    <a class="dropdown-item" href="/admin/viewUser.php">View all users</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/migration/createTable.php">Create Table</a>
                    <a class="dropdown-item" href="/migration/fixtures.php">Fixtures DB</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="students.php">Students</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="viewUsersByRole.php">Admins</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="teachers.php">Teacher</a>
            </li>
        </ul>
    </nav>

    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">First name</th>
            <th scope="col">Second Name</th>
            <th scope="col">Phone Number</th>
            <th scope="col">Role</th>
            <th scope="col">Working day</th>
            <th scope="col">Subject</th>
            <th scope="col">Average mark</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <?= $userObj->getVisitCard($userObj); ?>
            </tr>
        </tbody>
    </table>
</div>





<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
