<?php


trait getById{
    static public function getById($id, PDO $pdo){
        $id = htmlspecialchars($id);
        try {
            $sql = 'SELECt * FROM persons WHERE id=:id';
            $statement = $pdo->prepare($sql);
            $statement->bindValue(':id', $id);
            $statement->execute();
            $userArr = $statement->fetchAll();
            $userArr = $userArr[0];
            $userObjs = [];

            switch ($userArr['role']){
                case 'Admin':
                    $userObjs[] = new  Admin($userArr['first_name'], $userArr['second_name'], $userArr['phone'], $userArr['email'], $userArr['role'],$userArr['working_day']);
                    break;
                case 'Teacher':
                    $userObjs[] = new  Teacher($userArr['first_name'], $userArr['second_name'], $userArr['phone'],$userArr['email'], $userArr['role'],$userArr['subject']);
                    break;
                case 'Student':
                    $userObjs[] = new  Student($userArr['first_name'], $userArr['second_name'], $userArr['phone'],$userArr['email'], $userArr['role'],$userArr['average_mark']);
                    break;
            }
            foreach ($userObjs as $userObj) {
                $userObj->setId($id);
            }

            return $userObj;
        }catch (Exception $exception){
            header('Location:../404.php');
            die();
        }
    }
}
