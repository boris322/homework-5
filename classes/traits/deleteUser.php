<?php
trait deleteUser{
    static public function delete($id, PDO $pdo){
        $id = htmlspecialchars(intval($id));
        $user = self::getById($id, $pdo);
        $user->destroy($pdo);
    }

    public function destroy(PDO $pdo){
        try {
            $sql = "DELETE FROM persons WHERE id=:id";
            $statement = $pdo->prepare($sql);
            $statement->bindValue(':id', $this->id);
            $statement->execute();
        }catch (Exception $exception){
            header('Location:../404.php');
            die();
        }
    }
}