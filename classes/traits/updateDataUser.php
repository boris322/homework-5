<?php
trait updateDataUser{

    public function updateDataUser(PDO $pdo){
        try {
            $sql = 'UPDATE persons SET
        first_name = :first_name,
        second_name = :second_name,
        phone = :phone,
        email = :email,
        role = :role,
        average_mark = :average_mark,
        subject = :subject,
        working_day = :working_day
        WHERE id = :id
        ';

            $statement = $pdo->prepare($sql);

            $statement->bindValue(':id', $this->getId());
            $statement->bindValue(':first_name', $this->first_name);
            $statement->bindValue(':second_name', $this->secondName);
            $statement->bindValue(':phone', $this->phoneNumber);
            $statement->bindValue(':email', $this->email);
            $statement->bindValue(':role', $this->role);
            $statement->bindParam(':average_mark', $this->averageMark);
            $statement->bindValue(':subject', $this->subject);
            $statement->bindValue(':working_day', $this->dayWork);

            $statement->execute();

        }catch (Exception $exception){
            header('Location:../404.php');
            die();
        }
    }
}