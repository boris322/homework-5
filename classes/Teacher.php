<?php
require_once 'traits/getById.php';
require_once 'traits/deleteUser.php';
require_once 'traits/updateDataUser.php';
require_once 'Admin.php';
require_once 'Teacher.php';
require_once 'Student.php';

class Teacher extends Person
{
    use getById, deleteUser, updateDataUser;

    protected $subject = '';

    public function __construct($first_name, $secondName, $phoneNumber, $email, $role, $subject)
    {
        parent::__construct($first_name, $secondName, $phoneNumber, $email, $role);
        $this->subject = $subject;
    }

    public function getVisitCard(Person $userObj){
        $str = '';
        $str .= '<td>' . $userObj->first_name . '</td>';
        $str .= '<td>' . $userObj->secondName . '</td>';
        $str .= '<td>' . $userObj->phoneNumber . '</td>';
        $str .= '<td>' . $userObj->role . '</td>';
        $str .= '<td> </td>';
        $str .= '<td>' . $userObj->subject . '</td>';
        return $str;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    static public function getByRole($role, PDO $pdo){
        $role = htmlspecialchars($role);
        try {
            $sql = 'SELECt * FROM persons WHERE role= :role';
            $statement = $pdo->prepare($sql);
            $statement->bindValue(':role', $role);
            $statement->execute();
            $usersArr = $statement->fetchAll();
            $userObjs = [];

            foreach ($usersArr as $userArr){
                $userObj = new self($userArr['first_name'],$userArr['second_name'],$userArr['phone'],$userArr['email'],$userArr['role'],$userArr['subject']);
                $userObj->setId($userArr['id']);
                $userObjs[] = $userObj;
            }
            return $userObjs;

        }catch (Exception $exception){
            header('Location:404.php');
            die();
        }
    }

    static public function upgrade($id, $firstName, $secondName, $email, $phone, $role, $subject, PDO $pdo){
        $id = intval($id);
        $firstName = htmlspecialchars($firstName);
        $secondName = htmlspecialchars($secondName);
        $email = htmlspecialchars($email);
        $phone = htmlspecialchars($phone);
        $role = htmlspecialchars($role);

        $subject = htmlspecialchars($subject);

        $userObjs = new  Teacher($firstName, $secondName, $phone, $email, $role ,$subject);

        $userObjs->setId($id);

        $userObjs->updateDataUser($pdo);
    }

    static public function All(PDO $pdo){
        try {
            $role = 'Teacher';
            $sql = "SELECT id, first_name, second_name, role, working_day FROM persons WHERE role='$role'";
            $pdoResult = $pdo->query($sql);
            $usersArr = $pdoResult->fetchAll();

            $userObjs = [];
            foreach ($usersArr as $userArr){
                $userObj = new self($userArr['first_name'],$userArr['second_name'],$userArr['phone'],$userArr['email'],$userArr['role'], $userArr['working_day']);
                $userObj->setId($userArr['id']);
                $userObjs[] = $userObj;
            }
            return $userObjs;
        }catch (Exception $exception){
            //header('Location: 404.php');
            return $exception->getCode() . ' ' . $exception->getMessage();
            die();
        }
    }

    public function createUser(PDO $pdo){
        try {
            $sql = 'INSERT INTO persons SET
        first_name = :first_name,
        second_name = :second_name,
        phone = :phone,
        email = :email,
        role = :role,
        subject = :subject
        ';

            $statement = $pdo->prepare($sql);

            $statement->bindValue(':first_name', $this->first_name);
            $statement->bindValue(':second_name', $this->secondName);
            $statement->bindValue(':phone', $this->phoneNumber);
            $statement->bindValue(':email', $this->email);
            $statement->bindValue(':role', $this->role);
            $statement->bindValue(':subject', $this->subject);

            $statement->execute();

        }catch (Exception $exception){
            header('Location:../404.php');
            die();
        }
    }

}