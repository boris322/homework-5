<?php
require_once 'Admin.php';
require_once 'Student.php';
require_once 'Teacher.php';

class Person
{
    protected $first_name = '';

    protected $secondName = '';

    protected $phoneNumber;

    protected $email;

    protected $role = '';

    public function __construct($first_name, $secondName, $phoneNumber, $email, $role)
    {
        $this->first_name  = $first_name;
        $this->secondName  = $secondName;
        $this->phoneNumber = $phoneNumber;
        $this->email       = $email;
        $this->role        = $role;
    }


    public function setId($id){
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getFirstName()
    {
        return $this->first_name;
    }

    public function getSecondName()
    {
        return $this->secondName;
    }

    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getRole()
    {
        return $this->role;
    }
}